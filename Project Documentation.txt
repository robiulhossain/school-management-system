1. Student Management		##Sharmin
			-ApplicaionForm
			-Quota
			-Student
						
2. Class Routine  			##Benozir
			-ClassRoutine
			
3. Exam Routine  			##Imran
			-ExamRoutine
			-Exam
			
4. Exam Result  			##Robiul
		-Exam
		-ExamMarks
		-ExamResult
				
5. Teacher Management  		##Kawser
		-AttendanceTeacher
			
6. Staff Management-Staff	##Allawdin Ahsan
		-staffs
		-Task

										
7. Subject Management  		##Akram
		-Subject
		-Group			
		-Holiday		
		-Event		
		-Notice
		-RulesRegulations
								
8. Attendance  				##Rokeya
		-AttendanceStudent
										
9. System Admin  			##Zafor
		-branch 
		-Teacher
				
10-Brunch Admin  			##Zulhas
		-ClassRoom		
		-BrunchClass
		-Section
		-Room	
		-SchoolClass
		-SchoolVersion
		-shift
		




Some Common Roles:
	1- Database Context name : private readonly ApplicationDbContext _context;
	2- Defendency Injection  : private readonly IBrunchRepository _branchRepo; [_   + table name + Repo] 
	3- must user camel case  : parametter name [ examMark ],[branchId, branchName] 
	

Steps For upload your Code or File.
Step 1- notice our main group.
Step 2- sync your remote project or clone again remote project. 
Step 3- copy your code or file in new clone project or synced project.
Step 4- committ and sync your changes.
Step 5- Notice in group again.



1 - Home Page Common:	
				1- about Tab  :a-school name b-establish date c-Owner Details  d-History
				2- contact Tab :
				3- some Teacher info :
				4- Yearly best student section
				5- Apply Applicaion From
				6- All Brunch info with subject info
				7- Notice board
				8- public Holiday
				9- Event info
				10- Header & footer
				11- add rollsRegulation
				12- Login 
				13- 
				

2 - Admin Interface :
				1- create new shift with school varsion 
				2- add new Room, class and classRoom
				3- add or modify a subjects
				4- add new Teacher 
				5- add new staffs
				6- Add new Event
				7- Add new Notice
				8- create teacher rotine
				10- create class rotine
				11- create exam rotine
				12- Select Student from ApplicaionForm
				13- Take Attendence of teachers & staffs
				
3 - Teacher Interface :
				1- Teacher Profile & Modify & change password
				2- Teacher Rotine 
				3- Inser Exam Marks self subjects can use multi row insert.
				4- manage assigened student of section
				5- Scerch sficific student
				6- Take Attendence of students

4 - Student Interface :
				1- Student Profile & Modify & change password
				2- self Class Rotine 
				3- self exam Rotine
				4- can see self exam result by id 
				5- can see all class students result
				6- can see Attendence info
				7- can see all subject of student
				8- Can see all roll regulation 
				9- Can view student all info by report 
				10- 
				

5 - staffs Interface :
				1- Staff profile & Modify & change password
				2- can see Staff Rotine
				3- can see Attendence info
				4- can change password


Trainee         -> [Model-1]-[Model-2]-[Model-3]
===============================================================
Imran Hossen    ->[Teacher]-[ClassRoom]-[ClassRoutine]
Alauddin Ahsan	->[AdmissionApply]-[Quota]-[Shift]
Rokiya Akter	->[Division]-[District]-[Exam]
===============================================================
Sharmin Rumpa	->[Student]-[Designation]-[Brunch]
Akram Raj		->[Countrie]-[NoticeBoard]-[ExamRoutine]
Zulhas Mia		->[Holiday]-[Room]-[Event]
================================================================
Benojir Khanom	->[BrunchClass]-[Subject]-[ExamMarks]-[group]
Kawser Ahmed	->[PoliceStation]-[PostOffice]-[RulesRegulations]
Alauddin Japor	->[SchoolVersion]-[SchoolClass]-[Section]
=================================================================
Robiul Hossain	->[Staff]-[ExamResult]-[EducationSystem]