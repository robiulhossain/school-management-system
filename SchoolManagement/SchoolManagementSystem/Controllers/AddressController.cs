﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolManagementSystem.Data;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("[controller]/[action]")]
    //[Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        ApplicationDbContext _context;
        public AddressController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Country>> GetCountry()
        {
            try
            {
                var countryList =  _context.Country.ToList();

                if (countryList.Count() > 0)
                {
                    return Ok(countryList);
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }
        [HttpGet("{countryId}")]
        public ActionResult<IEnumerable<Division>> GetDivision(int countryId)
        {
            try
            {
                var List = _context.Division.Where(d => d.CountryId == countryId).ToList();

                if (List.Count() > 0)
                {
                    return Ok(List);
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }
        [HttpGet("{divisionId}")]
        public ActionResult<IEnumerable<District>> GetDistrict(int divisionId)
        {
            try
            {
                var List = _context.District.Where(d => d.DivisionId == divisionId).ToList();

                if (List.Count() > 0)
                {
                    return Ok(List);
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }
        [HttpGet("{districtId}")]
        public ActionResult<IEnumerable<PoliceStation>> GetPoliceStation(int districtId)
        {
            try
            {
                var List = _context.PoliceStation.Where(d => d.DistrictId == districtId).ToList();

                if (List.Count() > 0)
                {
                    return Ok(List);
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpGet("{policeStationId}")]
        public ActionResult<IEnumerable<PostOffice>> GetPostOffice(int policeStationId)
        {
            try
            {
                var List = _context.PostOffice.Where(d => d.PoliceStationId == policeStationId).ToList();

                if (List.Count() > 0)
                {
                    return Ok(List);
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

    }
}
