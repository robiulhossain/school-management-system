﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    //[Route("api/[controller]")]
    [Route("[controller]/[action]")]
    [ApiController]
    public class AttendanceController : ControllerBase
    {
        private readonly IAttendanceOfStudentRepository _studentAttendanceRepo;
        private readonly IAttendanceOfTeacherRepository _teacherAttendanceRepo;
        private readonly IAttendanceOfStaffReopsitory _staffAttendanceRepo;
        public AttendanceController(IAttendanceOfStudentRepository studentAttendanceRepo, IAttendanceOfTeacherRepository teacherAttendanceRepo, IAttendanceOfStaffReopsitory staffAttendanceRepo)
        {
            _studentAttendanceRepo = studentAttendanceRepo;
            _teacherAttendanceRepo = teacherAttendanceRepo;
            _staffAttendanceRepo = staffAttendanceRepo;

        }


       // [HttpGet]
        public IEnumerable<object> GetStudentInfo()
        {
            return _studentAttendanceRepo.GetStudentByNumber();
        }

        //[HttpPost("{id}")]
        public void SaveAttendance(int id)
        {
            if (id != 0)
            {
                _studentAttendanceRepo.SaveAttendance(id);                
            }
        }

        //[HttpGet("{id}")]
        public int GetStudentStatus(int id)
        {
            return _studentAttendanceRepo.GetStudentStatus(id);
        }

        //[HttpPut("{id}")]
        public void UpdateLeaveTime(int id)
        {
            _studentAttendanceRepo.UpdateForOutTime(id);        
        }

       // [HttpPut("{id}")]
        public ActionResult saveFinger(int id, string finger)
        {
            var response = _studentAttendanceRepo.registerStudentFinger(id, finger);

            if (response == 1)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                return Content("Finger Data Successfully saved.", System.Net.Mime.MediaTypeNames.Text.Plain);
            }
            Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            return Content("The attached file is not supported", System.Net.Mime.MediaTypeNames.Text.Plain);
        }

        //[HttpGet("{id}")]
        public ActionResult<string> GetStudentFingerData(int id)
        {
            var fingerData = _studentAttendanceRepo.GetStudentFingerData(id);

            if (fingerData != null)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                return Content(fingerData, System.Net.Mime.MediaTypeNames.Text.Plain);
            }
            Response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;
            return Content("Scerching Data is null", System.Net.Mime.MediaTypeNames.Text.Plain);
        }



        //***************************TeacherAttendance**********************************


       // [HttpGet]
        public IEnumerable<object> GetTeacherInfo()
        {
            return _teacherAttendanceRepo.GetTeacherByNumber();
        }

        //[HttpPost("{id}")]
        public void SaveTeacherAttendance(int id)
        {
            if (id != 0)
            {
                _teacherAttendanceRepo.SaveAttendance(id);
            }
        }

       // [HttpGet("{id}")]
        public int GetTeacherStatus(int id)
        {
            return _teacherAttendanceRepo.GetTeacherStatus(id);
        }

        //[HttpPut("{id}")]
        public void UpdateTeacherLeaveTime(int id)
        {
            _teacherAttendanceRepo.UpdateForOutTime(id);
        }

       // [HttpPut("{id}")]
        public ActionResult saveTeacherFinger(int id, string finger)
        {
            var response = _teacherAttendanceRepo.registerTeacherFinger(id, finger);

            if (response == 1)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                return Content("Finger Data Successfully saved.", System.Net.Mime.MediaTypeNames.Text.Plain);
            }
            Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            return Content("The attached file is not supported", System.Net.Mime.MediaTypeNames.Text.Plain);
        }

        //[HttpGet("{id}")]
        public ActionResult<string> GetTeacherFingerData(int id)
        {
            var fingerData = _teacherAttendanceRepo.GetTeacherFingerData(id);

            if (fingerData != null)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                return Content(fingerData, System.Net.Mime.MediaTypeNames.Text.Plain);
            }
            Response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;
            return Content("Scerching Data is null", System.Net.Mime.MediaTypeNames.Text.Plain);
        }


        //============================ Staff Attendance =========================================//

        [HttpGet]
        public IEnumerable<object> GetStaffInfo()
        {
            return _staffAttendanceRepo.GetStaffByNumber();
        }

        [HttpPost("{id}")]
        public void SaveStaffAttendance(int id)
        {
            if (id != 0)
            {
                _staffAttendanceRepo.SaveAttendance(id);
            }
        }

        [HttpGet("{id}")]
        public int GetStaffStatus(int id)
        {
            return _staffAttendanceRepo.GetStaffStatus(id);
        }

        [HttpPut("{id}")]
        public void UpdateStaffLeaveTime(int id)
        {
            _staffAttendanceRepo.UpdateForOutTime(id);
        }

        [HttpPut("{id}")]
        public ActionResult saveStaffFinger(int id, string finger)
        {
            var response = _staffAttendanceRepo.registerStaffFinger(id, finger);

            if (response == 1)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                return Content("Finger Data Successfully saved.", System.Net.Mime.MediaTypeNames.Text.Plain);
            }
            Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            return Content("The attached file is not supported", System.Net.Mime.MediaTypeNames.Text.Plain);
        }

        [HttpGet("{id}")]
        public ActionResult<string> GetStaffFingerData(int id)
        {
            var fingerData = _staffAttendanceRepo.GetStaffFingerData(id);

            if (fingerData != null)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                return Content(fingerData, System.Net.Mime.MediaTypeNames.Text.Plain);
            }
            Response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;
            return Content("Scerching Data is null", System.Net.Mime.MediaTypeNames.Text.Plain);
        }

    }
}
