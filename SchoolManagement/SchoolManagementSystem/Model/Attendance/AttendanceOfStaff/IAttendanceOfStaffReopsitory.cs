﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface IAttendanceOfStaffReopsitory
    {
        public IEnumerable<object> GetStaffByNumber(/*string teacherNumber*/);
        public string GetStaffFingerData(int id);
        public int registerStaffFinger(int id, string fingerdt);
        public int SaveAttendance(int teacherId);
        public int GetStaffStatus(int id);
        public void UpdateForOutTime(int id);
    }
}
