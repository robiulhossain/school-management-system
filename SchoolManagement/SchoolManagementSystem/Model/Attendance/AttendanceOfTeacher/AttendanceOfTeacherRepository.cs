﻿using SchoolManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class AttendanceOfTeacherRepository : IAttendanceOfTeacherRepository
    {
        ApplicationDbContext _context;
        public AttendanceOfTeacherRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<object> GetTeacherByNumber()
        {
            var teachers = _context.Teacher.Select(a => new { a.Id, a.FirstName  }).ToList();
            if (teachers != null)
            {
                return teachers;
            }
            return null;
        }

        public string GetTeacherFingerData(int id)
        {
            if (TeacherExists(id))
            {
                var finger = _context.Teacher.Where(s => s.Id == id).Select(a => a.FingerData).FirstOrDefault();
                return finger;
            }
            return null;
        }

        public int GetTeacherStatus(int id)
        {
            int outId = 0;
            string countEnrollment = _context.Teacher.Where(ta => ta.Id == id).Select(a => a.FingerData).FirstOrDefault();
            if (countEnrollment == null)
            {
                outId = -1;
            }
            else
            {
                int countAttendance = _context.AttendanceOfTeacher.Where(ta => ta.TeacherId == id && ta.Date == DateTime.UtcNow.AddHours(6).Date).Count();
                if (countAttendance > 0)
                {
                    outId = _context.AttendanceOfTeacher.Single(ta => ta.TeacherId == id && ta.Date == DateTime.UtcNow.AddHours(6).Date).Id;
                }
            }
            return outId;
        }

        public int registerTeacherFinger(int id, string fingerdt)
        {
            if (TeacherExists(id))
            {
                var teacher = _context.Teacher.Where(s => s.Id == id).FirstOrDefault();
                teacher.FingerData = fingerdt;
                _context.Teacher.Update(teacher);
                _context.SaveChanges();
                return 1;
            }
            return 0;
        }

        public int SaveAttendance(int teacherId)
        {
            TimeSpan time = DateTime.UtcNow.AddHours(6).TimeOfDay;
            AttendanceOfTeacher attendanceOfTeacher  = _context.AttendanceOfTeacher.Where(ta => ta.TeacherId == teacherId && ta.Date == DateTime.UtcNow.AddHours(6).Date).FirstOrDefault();

            if (attendanceOfTeacher == null)
            {
                AttendanceOfTeacher NewAttendanceOfTeacher = new AttendanceOfTeacher { TeacherId = teacherId, Date = DateTime.UtcNow.AddHours(6).Date, InTime = time, OutTime = time };
                _context.AttendanceOfTeacher.Add(NewAttendanceOfTeacher);
                _context.SaveChanges();
                return 1;
            }
            return 0;
        }

        public void UpdateForOutTime(int id)
        {
            AttendanceOfTeacher attendanceOfTeacher = _context.AttendanceOfTeacher.Find(id);

            attendanceOfTeacher.OutTime = DateTime.UtcNow.AddHours(6).TimeOfDay;
            _context.AttendanceOfTeacher.Update(attendanceOfTeacher);
            _context.SaveChanges();
        }

        private bool TeacherExists(int id)
        {
            return _context.Teacher.Any(e => e.Id == id);
        }
    }
}
