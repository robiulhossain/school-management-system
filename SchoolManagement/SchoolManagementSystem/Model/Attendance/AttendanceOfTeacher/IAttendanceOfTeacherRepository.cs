﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface IAttendanceOfTeacherRepository
    {
        public IEnumerable<object> GetTeacherByNumber(/*string teacherNumber*/);
        public string GetTeacherFingerData(int id);
        public int registerTeacherFinger(int id, string fingerdt);
        public int SaveAttendance(int teacherId);
        public int GetTeacherStatus(int id);
        public void UpdateForOutTime(int id);
    }
}
